import { ContainerSkills, ContainerImages, Animation } from "./style";
import Its from "../../image/ts.svg";
import Icss from "../../image/sql.svg";
import Idocker from "../../image/docker.svg";
import Ireact from "../../image/react.svg";
import Idotnet from "../../image/dotnet.svg";
import Iredis from "../../image/redis.svg";
import Inode from "../../image/node.svg";

import Igit from "../../image/git.svg";
import { useEffect, useState } from "react";
import { useObserver } from "../../services/intersectionObserver";

export default function Skills() {
  const techs = [
    { title: "Microsoft.NET", icon: Idotnet },
    { title: "SQL Server", icon: Icss },
    { title: "Redis", icon: Iredis },
    { title: "Docker", icon: Idocker  },
    { title: "TypeScript", icon: Its },
    { title: "React", icon: Ireact },
    { title: "Node", icon: Inode },
    { title: "Git", icon: Igit },
  ];
  const [isShown, setisShown] = useState(false);

  const loadComponent = useObserver(setisShown);

  useEffect(() => {
    loadComponent.observe(document.getElementById("Skills"));
  }, []);

  return (
    <ContainerSkills id="Skills">
      <Animation isShown={isShown}>
        <h1>Tecnologias Exploradas</h1>

        <ContainerImages>
          {techs.map((tech, key) => (
            <div key={key}>
              <img src={tech.icon} alt={tech.title} />
              <figcaption>{tech.title}</figcaption>
            </div>
          ))}
        </ContainerImages>
      </Animation>
    </ContainerSkills>
  );
}
